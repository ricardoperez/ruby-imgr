# frozen_string_literal: true

module Imgr
  class NumberOfArgumentsError < StandardError; end

  class NotANumberError < StandardError
    def message
      'X and Y arguments should be a number'
    end
  end

  class EqualsZeroError < StandardError
    def message
      'X and Y arguments should not be zero'
    end
  end

  class NotAColorError < StandardError; end
end
