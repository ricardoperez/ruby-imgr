# frozen_string_literal: true

require 'imgr/commands/start_canvas'
require 'imgr/commands/fill_position'
require 'imgr/commands/fill_vertical'
require 'imgr/commands/fill_horizontal'
require 'imgr/commands/fill_region'

module Imgr
  class CommandParser
    COMMANDS = {
      'I' => { arguments: 3 },
      'S' => { arguments: 1 },
      'C' => { arguments: 1 },
      'L' => { arguments: 4, class: Imgr::FillPosition },
      'V' => { arguments: 5, class: Imgr::FillVertical },
      'H' => { arguments: 5, class: Imgr::FillHorizontal },
      'F' => { arguments: 4, class: Imgr::FillRegion }
    }.freeze

    # TODO: validate intput
    # 2. check is the correct argument, in right order
    # 4. validate color
    def initialize
      @canvas = nil
    end

    def start
      puts usage_msg
      loop do
        print('> ')
        cmd = $stdin.gets.chomp
        break if cmd == 'X'

        parse(cmd)
      rescue StandardError => e
        puts e.message
      end
    end

    def usage_msg
      <<~MSG
        IMGR:

        I M N Create a new M x N image with all pixels coloured white (O).
        C Clears the table, setting all pixels to white (O).
        L X Y C Colours the pixel (X,Y) with colour C.
        V X Y1 Y2 C Draw a vertical line of colour C in column X between rows Y1 and Y2 (inclusive).
        H X1 X2 Y C Draw a horizontal line of colour C in row Y between columns X1 and X2 (inclusive).
        F X Y C Fill the region R with the colour C.R is defined as:

        Pixel (X,Y) belongs to R.
        Any other pixel which is the same colour as (X,Y)
        and shares a common side with any pixel in R also belongs to this region.

        S Show the contents of the current image
        X Terminate the session
      MSG
    end

    def parse(cmd)
      args = cmd.split(' ')

      if COMMANDS.keys.include?(args[0])
        select_command(args)
      else
        puts '==> Invalid Command!!'
      end
    end

    # rubocop:disable Metrics/MethodLength
    def select_command(args)
      case args[0]
      when 'I'
        @canvas = StartCanvas.execute(args)
      when 'S'
        show
      when 'C'
        reset
      else
        do_command(args) do
          COMMANDS[args[0]][:class].execute(@canvas, args)
        end
      end
    end
    # rubocop:enable Metrics/MethodLength

    def do_command(cmd_args)
      command = COMMANDS[cmd_args[0]]
      number_arguments = command[:arguments]

      if number_arguments != cmd_args.count
        puts arguments_error_msg(cmd_args, number_arguments)
        return false
      end

      yield
    end

    def arguments_error_msg(cmd_args, number_arguments)
      [
        "==> Wrong number of arguments for #{cmd_args[0]}: ",
        "should be #{number_arguments} instead of #{cmd_args.count}"
      ].join('')
    end

    def show
      return if canvas_nil?

      puts <<~PRINT

        =>
        #{@canvas.draw}
      PRINT
    end

    def reset
      @canvas.reset unless canvas_nil?
    end

    def canvas_nil?
      if @canvas.nil?
        puts '-> Canvas not initilized: Type I H W to start.'
        true
      else
        false
      end
    end

    def canvas_not_started?
      @canvas.nil?
    end

    def self.start
      CommandParser.new.start
    end
  end
end
