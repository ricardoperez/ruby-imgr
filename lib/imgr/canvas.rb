# frozen_string_literal: true

module Imgr
  class Canvas
    attr_reader :board

    def initialize(width, height)
      @width = width
      @height = height
      @board = Array.new(height) { Array.new(width, 'O') }
    end

    def reset
      @board.each do |row|
        row.fill('O', 0, @width)
      end
    end

    def fill(pos_x, pos_y, color)
      @board[pos_y.to_i - 1][pos_x.to_i - 1] = color
    end

    def fill_horizontal(pos_x1, pos_x2, pos_y, color)
      row = @board[pos_y.to_i - 1]
      row.fill(color, (pos_x1.to_i - 1)..(pos_x2.to_i - 1))
    end

    def fill_vertical(pos_x, pos_y1, pos_y2, color)
      sub_board = @board[(pos_y1.to_i - 1)..(pos_y2.to_i - 1)]

      sub_board.each do |row|
        row[pos_x.to_i - 1] = color
      end
    end

    def fill_region(pos_x, pos_y, new_color)
      old_color = @board[pos_x.to_i][pos_y.to_i]

      @board.each do |row|
        row.map! { |val| val == old_color ? new_color : val }
      end
    end

    def draw
      @board.reduce('') do |acc, row|
        acc + row.reduce(:+) + "\n"
      end
    end

    def print
      puts draw
    end
  end
end
