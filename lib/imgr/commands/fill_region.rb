# frozen_string_literal: true

module Imgr
  class FillRegion
    include Imgr::Command

    attr_reader :canvas, :command_args

    def initialize(canvas, args)
      @canvas = canvas
      @number_of_arguments = 3
      @command_args = args[1..3]
    end

    def execute
      valid?

      canvas.fill_region(*command_args)
    end
  end
end
