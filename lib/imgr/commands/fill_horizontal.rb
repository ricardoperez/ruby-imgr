# frozen_string_literal: true

module Imgr
  class FillHorizontal
    include Imgr::Command

    attr_reader :canvas, :command_args

    def initialize(canvas, args)
      @canvas = canvas
      @number_of_arguments = 4
      @command_args = args[1..4]
    end

    def execute
      valid?

      canvas.fill_horizontal(*command_args)
    end

    def valid?
      pos_x1 = command_args[0]
      pos_x2 = command_args[1]

      raise ArgumentError, 'X1 can\'t be > than X2' if pos_x1.to_i > pos_x2.to_i

      super
    end
  end
end
