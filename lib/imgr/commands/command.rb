# frozen_string_literal: true

module Imgr
  module Command
    CHECK_ARG_NUMBER_RE = /\d/.freeze
    CHECK_ARG_COLOR_RE = /\A[A-Z]\Z/.freeze

    def self.included(klass)
      klass.extend ClassMethods
    end

    def eql_zero?(arg)
      arg.to_i.zero?
    end

    def not_number?(arg)
      CHECK_ARG_NUMBER_RE !~ arg
    end

    def not_color?(arg)
      CHECK_ARG_COLOR_RE !~ arg
    end

    def valid?
      raise Imgr::NumberOfArgumentsError, 'Number of arguments' if number_of_arguments_valid?

      *pixels, color = *command_args

      pixels.each do |pixel|
        raise Imgr::NotANumberError if not_number?(pixel)
        raise Imgr::EqualsZeroError if eql_zero?(pixel)
      end

      raise Imgr::NotAColorError, "#{color} Is not a valid color" if not_color?(color)

      true
    end

    def number_of_arguments_valid?
      @command_args.count != @number_of_arguments
    end

    # Helper to initialize and execute command
    module ClassMethods
      def execute(canvas, args)
        new(canvas, args).execute
      end
    end
  end
end
