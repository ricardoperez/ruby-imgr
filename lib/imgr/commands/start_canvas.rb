# frozen_string_literal: true

module Imgr
  class StartCanvas
    include Command

    def initialize(args)
      @heigth = args[1]
      @width = args[2]
    end

    def execute
      return unless valid?

      puts 'Initialize Canvas'
      Canvas.new(@heigth.to_i, @width.to_i)
    end

    def valid?
      if not_number?(@heigth) || not_number?(@width)
        puts 'Second or Third argument should be a number.'
        return false
      end

      if eql_zero?(@heigth) || eql_zero?(@width)
        puts 'Second or Third argument should not be zero.'
        return false
      end

      true
    end

    def self.execute(args)
      new(args).execute
    end
  end
end
