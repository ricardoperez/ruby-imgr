# frozen_string_literal: true

module Imgr
  class FillVertical
    include Imgr::Command

    attr_reader :canvas, :command_args

    def initialize(canvas, args)
      @canvas = canvas
      @number_of_arguments = 4
      @command_args = args[1..4]
    end

    def execute
      return unless valid?

      @canvas.fill_vertical(*command_args)
    end

    def valid?
      pos_y1 = command_args[1]
      pos_y2 = command_args[2]
      super

      raise ArgumentError, 'Y1 can\'t  be > Y2' if pos_y1.to_i > pos_y2.to_i

      true
    end
  end
end
