# frozen_string_literal: true

require 'imgr/version'
require 'imgr/canvas'
require 'imgr/exceptions'
require 'imgr/commands/command'
require 'imgr/command_parser'

module Imgr
  # Your code goes here...
  def self.start
    CommandParser.start
  end
end
