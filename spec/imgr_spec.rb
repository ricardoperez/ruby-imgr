# frozen_string_literal: true

RSpec.describe Imgr do
  it 'has a version number' do
    expect(Imgr::VERSION).not_to be nil
  end
end
