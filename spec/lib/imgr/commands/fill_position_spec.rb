# frozen_string_literal: true

module Imgr
  RSpec.describe FillPosition do
    describe '#execute' do
      let(:canvas) { StartCanvas.execute(%w[I 5 6]) }
      subject(:command) { described_class.execute(canvas, arguments) }

      context 'when arguments are correct' do
        let(:arguments) { %w[L 2 2 X] }

        it 'retuns filled position' do
          command
          expect(canvas.board[1][1]).to eq('X')
          expect(canvas.board[2][2]).to eq('O')
        end
      end

      context 'when arguments are invalid' do
        context 'when X or Y args are not numbers' do
          let(:arguments) { %w[L A A X] }

          it do
            expect { command }.to raise_error(Imgr::NotANumberError)
          end
        end

        context 'when X or Y args are zero' do
          let(:arguments) { %w[L 0 0 C] }

          it do
            expect { command }.to raise_error(Imgr::EqualsZeroError)
          end
        end

        context 'when color is not correct' do
          let(:arguments) { %w[L 1 2 CC] }

          it do
            expect { command }.to raise_error(Imgr::NotAColorError)
          end
        end
      end
    end
  end
end
