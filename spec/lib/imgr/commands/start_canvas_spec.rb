# frozen_string_literal: true

module Imgr
  RSpec.describe StartCanvas do
    describe '#execute' do
      context 'with right arguments' do
        it 'retuns new canvas' do
          canvas = described_class.execute(%w[I 5 6])
          expect(canvas).to be_an_instance_of(Canvas)
        end
      end

      context 'with invalid arguments' do
        it 'return nil when height or width are not numbers' do
          canvas = described_class.execute(%w[I A A])
          expect(canvas).to be(nil)
        end

        it 'prints error message when height or width args are not numbers' do
          expect(STDOUT).to receive(:puts).with('Second or Third argument should be a number.')

          described_class.execute(%w[I A A])
        end

        it 'prints error message when height or width args are zero' do
          expect(STDOUT).to receive(:puts).with('Second or Third argument should not be zero.')

          described_class.execute(%w[I 0 0])
        end
      end
    end
  end
end
