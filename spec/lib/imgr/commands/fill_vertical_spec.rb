# frozen_string_literal: true

module Imgr
  RSpec.describe FillVertical do
    describe '#execute' do
      subject(:command) { described_class.execute(canvas, arguments) }

      let(:canvas) { StartCanvas.execute(%w[I 5 6]) }

      context 'with right arguments' do
        it 'fill a vertical line' do
          canvas_obj = canvas
          described_class.execute(canvas_obj, %w[V 1 1 5 X])

          expect(canvas.board[1][0]).to eq('X')
          expect(canvas.board[2][0]).to eq('X')
          expect(canvas.board[3][0]).to eq('X')
          expect(canvas.board[4][0]).to eq('X')
          expect(canvas.board[5][0]).to eq('O')
        end
      end

      context 'when arguments are invalid' do
        let(:arguments) { %w[V 1 2 3] }

        context 'when number the arguments are wrong' do
          it do
            expect { subject }.to raise_error(Imgr::NumberOfArgumentsError)
          end
        end

        context 'when X or Y arguments are not number' do
          let(:arguments) { %w[V 1 2 A C] }

          it 'raises error' do
            expect { command }.to raise_error(Imgr::NotANumberError)
          end
        end

        context 'when X or Y args are zero' do
          let(:arguments) { %w[L 0 0 0 C] }

          it 'raises error' do
            expect { command }.to raise_error(Imgr::EqualsZeroError)
          end
        end

        context 'when color is not correct' do
          let(:arguments) { %w[L 1 2 3 CC] }

          it 'raises error' do
            expect { command }.to raise_error(Imgr::NotAColorError)
          end
        end
      end
    end
  end
end
