# frozen_string_literal: true

module Imgr
  RSpec.describe FillHorizontal do
    describe '#execute' do
      subject(:command) { described_class.execute(canvas, arguments) }

      let(:canvas) { StartCanvas.execute(%w[I 5 6]) }

      context 'when arguments are correct' do
        let(:arguments) { %w[H 1 4 1 X] }

        it 'fill a horizontal line' do
          command
          expect(canvas.board[0][0]).to eq('X')
          expect(canvas.board[0][1]).to eq('X')
          expect(canvas.board[0][2]).to eq('X')
          expect(canvas.board[0][3]).to eq('X')
          expect(canvas.board[0][4]).to eq('O')
        end
      end

      context 'when arguments are invalid' do
        context 'when arguments are missing' do
          let(:arguments) { %w[H 2 3] }

          it 'raise exception' do
            expect { command }.to raise_error(Imgr::NumberOfArgumentsError)
          end
        end

        context 'when X or Y arguments are not number' do
          let(:arguments) { %w[L 1 2 A C] }

          it do
            expect { command }.to raise_error(Imgr::NotANumberError)
          end
        end

        context 'when X or Y args are zero' do
          let(:arguments) { %w[L 0 0 0 C] }

          it do
            expect { command }.to raise_error(Imgr::EqualsZeroError)
          end
        end

        context 'when color is not correct' do
          let(:arguments) { %w[L 1 2 3 CC] }

          it do
            expect { command }.to raise_error(Imgr::NotAColorError)
          end
        end

        context 'when second arg is bigger than third' do
          let(:arguments) { %w[L 3 2 3 C] }

          it do
            expect { command }.to raise_error(ArgumentError, 'X1 can\'t be > than X2')
          end
        end
      end
    end
  end
end
