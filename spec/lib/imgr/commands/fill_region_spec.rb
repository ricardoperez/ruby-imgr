# frozen_string_literal: true

module Imgr
  RSpec.describe FillRegion do
    describe '#execute' do
      let(:canvas) { StartCanvas.execute(%w[I 5 6]) }
      subject(:command) { described_class.execute(canvas, arguments) }

      context 'when arguments are valid' do
        let(:arguments) { %w[F 1 1 X] }

        it 'retuns filled region' do
          command

          expect(canvas.board[0][0]).to eq('X')
          expect(canvas.board[4][4]).to eq('X')
        end
      end

      context 'when arguments are invalid' do
        context 'when X or Y args are not numbers' do
          let(:arguments) { %w[F A A R] }

          it do
            expect { command }.to raise_error(Imgr::NotANumberError)
          end
        end

        context 'when X or Y args are zero' do
          let(:arguments) { %w[F 0 0 C] }

          it do
            expect { command }.to raise_error(Imgr::EqualsZeroError)
          end
        end

        context 'when color is not correct' do
          let(:arguments) { %w[F 1 2 CC] }

          it do
            expect { command }.to raise_error(Imgr::NotAColorError)
          end
        end
      end
    end
  end
end
