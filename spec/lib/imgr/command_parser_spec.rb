# frozen_string_literal: true

module Imgr
  RSpec.describe CommandParser do
    subject(:command) { described_class.new }

    describe '#parse' do
      subject(:parse) { command.parse(arguments_string) }

      context 'when the command `I` is insert' do
        let(:arguments_string) { 'I 5 6' }

        it 'call initialize canvas' do
          expect_any_instance_of(StartCanvas).to receive(:execute)
          parse
        end
      end

      context 'when the command `L` is insert' do
        let(:arguments_string) { 'L 5 6 C' }

        it 'call fill position' do
          expect_any_instance_of(FillPosition).to receive(:execute)
          parse
        end
      end

      context 'when the command `F` is insert' do
        let(:arguments_string) { 'F 5 6 R' }
        it 'call fill region' do
          expect_any_instance_of(FillRegion).to receive(:execute)
          parse
        end
      end

      context 'when the command `V` is insert' do
        let(:arguments_string) { 'V 2 3 4 R' }

        it 'call fill vertical' do
          expect_any_instance_of(FillVertical).to receive(:execute)
          parse
        end
      end

      context 'when the command `H` is insert' do
        let(:arguments_string) { 'H 2 3 4 R' }

        it 'call fill horizontal' do
          expect_any_instance_of(FillHorizontal).to receive(:execute)
          parse
        end
      end

      context 'when error happen' do
        context 'when calling any command without intializing canvas' do
          it do
            expect(STDOUT).to receive(:puts).with('-> Canvas not initilized: Type I H W to start.')
            command.parse('S')
          end
        end

        context 'when not listed commands is inserted' do
          let(:arguments_string) { 'G 5 6 C' }

          it do
            expect(STDOUT).to receive(:puts).with('==> Invalid Command!!')
            parse
          end
        end

        context 'when the numbers are arguments wrong ' do
          let(:arguments_string) { 'H 5 6 C' }
          let(:expected_msg) do
            '==> Wrong number of arguments for H: should be 5 instead of 4'
          end

          it do
            expect(STDOUT).to receive(:puts).with(expected_msg)
            parse
          end
        end
      end
    end
  end
end
