# frozen_string_literal: true

module Imgr
  RSpec.describe Canvas do
    context 'Start the canvas' do
      it 'has empty board' do
        canvas = described_class.new(5, 6)

        expected_output = <<~OUTPUT
          OOOOO
          OOOOO
          OOOOO
          OOOOO
          OOOOO
          OOOOO
        OUTPUT

        expect(canvas.draw).to eq(expected_output)
      end
    end

    context 'Insert values/color to the canvas' do
      it 'fills a specific possition in the board' do
        canvas = described_class.new(4, 4)

        canvas.fill(2, 3, 'A')

        expected_output = <<~OUTPUT
          OOOO
          OOOO
          OAOO
          OOOO
        OUTPUT

        expect(canvas.draw).to eq(expected_output)
      end

      it 'fills a horizontal line given the column 2,4 and line 3' do
        canvas = described_class.new(5, 4)

        canvas.fill_horizontal(2, 4, 3, 'Z')

        expected_output = <<~OUTPUT
          OOOOO
          OOOOO
          OZZZO
          OOOOO
        OUTPUT

        expect(canvas.draw).to eq(expected_output)
      end

      it 'fills a vertical line given the column 2 and lines 2,4' do
        canvas = described_class.new(3, 6)

        canvas.fill_vertical(2, 2, 4, 'X')

        expected_output = <<~OUTPUT
          OOO
          OXO
          OXO
          OXO
          OOO
          OOO
        OUTPUT

        expect(canvas.draw).to eq(expected_output)
      end
    end

    context 'Doing multiples change in the image' do
      it 'return the filled canvas' do
        canvas = described_class.new(5, 6)

        canvas.fill(2, 3, 'A')
        canvas.fill_region(3, 3, 'J')
        canvas.fill_vertical(2, 3, 4, 'W')
        canvas.fill_horizontal(3, 4, 2, 'Z')

        expected_output = <<~OUTPUT
          JJJJJ
          JJZZJ
          JWJJJ
          JWJJJ
          JJJJJ
          JJJJJ
        OUTPUT

        expect(canvas.draw).to eq(expected_output)
      end
    end
  end
end
