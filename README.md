# Imgr

Your task is to write a simple image editor program in ruby. Avoid using any external gems unless they're for testing purposes. You should produce a program that can be run from the command line and interactively accept and respond to commands. The editor should support the following commands:


1. I M N Create a new M x N image with all pixels coloured white (O).
1. C Clears the table, setting all pixels to white (O).
1. L X Y C Colours the pixel (X,Y) with colour C.
1. V X Y1 Y2 C Draw a vertical line of colour C in column X between rows Y1 and Y2 (inclusive).
1. H X1 X2 Y C Draw a horizontal line of colour C in row Y between columns X1 and X2 (inclusive).
1. F X Y C Fill the region R with the colour C.R is defined as:
    1. Pixel (X,Y) belongs to R. 
    1. Any other pixel which is the same colour as (X,Y)
    1. and shares a common side with any pixel in R also belongs to this region.
1. S Show the contents of the current image
1. X Terminate the session

## Usage

To start the Imgr terminal(developed using 2.3.1):

```sh
-$ cd ~/path/to/ruby-imgr 
-$ bin/imgr
 >
```

## Installation

Clone this repo to your local:

And then execute:

```
-$ bin/setup
```

or  execute:

```
-$ bundle
```

## Example

Example inputs and outputs:

```
> I 5 6
> L 2 3 A
> S

=>
OOOOO
OOOOO
OAOOO
OOOOO
OOOOO
OOOOO

> F 3 3 J
> V 2 3 4 W
> H 3 4 2 Z
> S

=>
JJJJJ
JJZZJ
JWJJJ
JWJJJ
JJJJJ
JJJJJ
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/imgr.
